// retrieve the user information for is Admin
let adminUser = localStorage.getItem("isAdmin");
let coursesContainer = document.querySelector("#coursesContainer");
console.log(adminUser);

//will contain the html for the different buttons per user
let cardFooter;

//walang need ipasa kaya url lang sa fetch
fetch('https://stark-hamlet-12332.herokuapp.com/api/courses')
.then(res =>res.json())
.then(data => {
	console.log(data);

	//variable to store the card/message to show if there's no courses
	let courseData;

	if(data.length < 1){
		courseData = "No courses available"

	} else {
		courseData = data.map(course =>{

			if((adminUser == 'true') ||((adminUser == 'false' || !adminUser) && course.isActive==true) ){

				if(adminUser == 'false'){
					cardFooter	= 
					`
						<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				} else if (!adminUser){
					cardFooter	= 
					`
						<a href="./login.html" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					`
				} else {
					if (course.isActive==true){
						action=`<button type="button" class="btn btn-danger text-white btn-block" data-val=${course._id} data-name =${course.name} data-status = ${course.isActive} data-toggle="modal" data-target="#statusConfirmation"> Disable Course </button>`
					}else{
						action=`<button type="button" class="btn btn-success text-white btn-block" data-val=${course._id} data-name =${course.name} data-toggle="modal" data-target="#statusConfirmation"> Enable Course </button>`
					}

					cardFooter	=
					`
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block editButton"> Select Course </a>
					${action}
					<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-secondary text-white btn-block editButton"> Edit </a>
	                `
				}

				
				return(
					`
					<div class="col-md-6 my-3">
	                    <div class='card' id=${course._id}>
	                        <div class='card-body'>
	                            <h5 class='card-title'>${course.name}</h5>
	                            <p class='card-text text-left'>
	                                ${course.description}
	                            </p>
	                            <p class='card-text text-right'>
	                               ₱ ${course.price}
	                            </p>

	                        </div>
	                        <div class='card-footer'>
	                            ${cardFooter}
	                        </div>
	                    </div>
	                </div>
					`
				)
			}
		}).join("")




		coursesContainer.innerHTML = courseData;

	}

//DISABLE CONFIRMATION MODAL

		coursesContainer.innerHTML+=
		`
			<div class="modal fade" id="statusConfirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div id="disableModalBody" class="modal-body">
						</div>
						<div id="disableModalFooter" class="modal-footer">
						</div>
					</div>
				</div>
			</div>
		`

	$('#statusConfirmation').on('show.bs.modal', function (event) {
		var courseId = $(event.relatedTarget).data('val');
		var courseName = $(event.relatedTarget).data('name');
		var courseStatus = $(event.relatedTarget).data('status');
		console.log(courseStatus)
		let disableModalBody = this.querySelector("#disableModalBody");
		let disableModalFooter = this.querySelector("#disableModalFooter");

		
		
		if(courseStatus==true){
			disableModalBody.innerHTML=`Are you sure you want to disable ${courseName} course?`
			disableModalFooter.innerHTML=
				`
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<a href="./deleteCourse.html?courseId=${courseId}" value=${courseId} class="btn btn-danger text-white px-4"> Yes </a>
				`
		}else{
			disableModalBody.innerHTML=`Are you sure you want to enable ${courseName} course?`
			disableModalFooter.innerHTML=
				`
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<a href="./enableCourse.html?courseId=${courseId}" value=${courseId} class="btn btn-success	 text-white px-4"> Yes </a>
				`
		}
	});

})



let modalButton = document.querySelector('#adminButton')

if(adminUser == 'false' || !adminUser){
		modalButton.innerHTML = null;
}else{
		modalButton.innerHTML = 
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block btn-primary"> Add Course </a>
			</div>
		`
}