let formSubmit = document.querySelector('#createCourse');


formSubmit.addEventListener("submit",(e) =>{

	e.preventDefault()


	let courseName = document.querySelector("#courseName").value
	let courseDescription = document.querySelector("#courseDescription").value
	let coursePrice = document.querySelector("#coursePrice").value

	let token = localStorage.getItem("token");


	fetch('https://stark-hamlet-12332.herokuapp.com/api/courses',{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name:courseName,
				description: courseDescription,
				price: coursePrice	
			})
	})
	.then(res => res.json())
	.then(data =>{
		console.log(data);
	})
})