let token = localStorage.getItem("token");




let profileContainer = document.querySelector('#profileContainer')
	// profileContainer.innerHTML=
	// `
	// 	<div>
	// 		<button type="button" class="btn btn-secondary" >Edit</button>
	// 	</div>
	// `

fetch('https://stark-hamlet-12332.herokuapp.com/api/users/details',{
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	},
})
.then(res => res.json())
.then(data =>{


	let	profile = `
			<div class="d-flex flex-column align-items-center col-12">
				<h3>First Name: ${data.firstName}</h3>
				<h3>Last Name: ${data.lastName}</h3>
				<h3>Mobile Number: ${data.mobileNo}</h3>
			</div>
			<div class='container text-center py-5' id='courseContainer'>
			</div>

		`


	profileContainer.innerHTML = profile

	let courseContainer = document.querySelector('#courseContainer')


	if(data.isAdmin==false){
		if (data.enrollments.length==0){

			courseContainer.innerHTML = 
				`
				
				<h5>No course yet. <a href="courses.html">Enroll Now!</a></h5>
				</div>
				`

		}else{
			courseContainer.innerHTML = `<h5 class="my-5"><span class="spinner-border text-primary"></span> Fetching Courses...</h5>`
			let courseNames =[]
			data.enrollments.map(course =>{
				courseNames.push(
					fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses/${course.courseId}`)
					.then(res => res.json())
					.then(data =>{
						return(
							`
							<tr>
								<td>${data.name}</td>
								<td>${course.enrolledOn.slice(0, 10)}</td>
								<td>${course.status}</td>
							</tr>
							`	
						)
					})
				)
			})


			Promise.all(courseNames)
			.then(function handleData(data){

				courseRow=data.join("")

				courseTable = 
					`
						<div class="col-12">
							<h4>Class History<h4>
							<table style="width:80%" align="center" id="classHistory">
							  	<tr>
							  		<th>Course</th>
							  		<th>Enrolled On</th>
							  		<th>Status</th>
							  	</tr>
							  	${courseRow}
						  	<table>
						<div>
					`
				courseContainer.innerHTML= courseTable
			})
			.catch(function handleError(error){
				console.log(error)
				alert("something went wrong")
			})
			



			
		/*// Working code but not sorted
			let courseNames=[]

			enrollmentLength=data.enrollments.length
			enrolleeCounter=0
			data.enrollments.map((course,index) =>{

				console.log(course.courseId)
				fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses/${course.courseId}`)
				.then(res => res.json())
				.then(data =>{
					courseNames.push(
						`
						${index}
						<tr>
							<td>${data.name}</td>
							<td>${course.enrolledOn}</td>
							<td>${course.status}</td>
						</tr>
						`
					)

					
					enrolleeCounter++	

					if( enrollmentLength == enrolleeCounter){
					console.log(courseNames)
					courseNames.sort(function (a, b) {
		  				return a.value - b.value;
					})
						courseTable = 
							`
								<div class="col-12">
									<h4>Class History<h4>
									<table style="width:80%" id="classHistory">
									  	<tr>
									  		<th>Course</th>
									  		<th>Enrolled On</th>
									  		<th>Status</th>
									  	</tr>
									  	${courseNames.join("")}
								  	<table>
								<div>
							`

						console.log(courseTable)
						profileContainer.innerHTML += courseTable
					}
					
					
				})

				
			})*/
		}
	}
})