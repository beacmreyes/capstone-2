console.log(window.location.search);



// instantiate a URLSearchParams object so we can execute methods to access parameters
let params = new URLSearchParams(window.location.search)

console.log(params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));


let courseId = params.get('courseId')

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let courseDetails = document.querySelector("#courseDetails")




let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin");

fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses/${courseId}`)
.then(res =>res.json())
.then(data =>{

	console.log(data);
	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;


	if(adminUser == 'false' || !adminUser){	
		

		enrollContainer.innerHTML =
			`
			<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>
			`

		document.querySelector("#enrollButton").addEventListener("click",() =>{
			fetch('https://stark-hamlet-12332.herokuapp.com/api/users/enroll',{
				method:'POST',
				headers:{
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data =>{
				console.log(data);


				if(data === true){

						alert('Thank you for enrolling! See you in class!');
						window.location.replace("courses.html")

				} else {
					alert("Something went wrong")
				}
			})


		})

	}else{
		if(data.isActive==true)
			enrollContainer.innerHTML =`<p>Current Status: Offered</p>`
		else{
			enrollContainer.innerHTML =`<p>Current Status: Not Offered</p>`
		}

		courseDetails.innerHTML +=
		`
		<div class="container text-center">
			<div class='text-center'>
					<h5>Enrollees : ${data.enrollees.length}</h5>
			</div>
			<div id="enrolleeContainer" class='container text-center'></div>
		</div>
		`

		if(data.enrollees.length>0){
			let enrolleeContainer = document.querySelector("#enrolleeContainer")

			enrolleeContainer.innerHTML	 =`<h5 class="my-5"><span class="spinner-border text-primary"></span> Fetching Enrollees...</h5>`
			
			let enrolleeList=[]
			data.enrollees.map(enrollee =>{
			 	enrolleeList.push(
			 		fetch(`https://stark-hamlet-12332.herokuapp.com/api/users/${enrollee.userId}`,{
			 			headers:{
							'Content-Type': 'application/json',
							'Authorization': `Bearer ${token}`
						}	
			 		})
					.then(res => res.json())
					.then(data =>{
						return(
							`
							<tr>
								<td>${data.firstName} ${data.lastName} </td>
								<td>${enrollee.enrolledOn.slice(0, 10)}</td>
							</tr>
							`					
						)
					})
		 		)	

			})



			Promise.all(enrolleeList)
			.then(function handleData(data){
				enrolleeRows = data.join("")
				enrolleeTable=`

					<table style="width:60%" align="center">
					  	<tr>
					  		<th>Student Name</th>
					  		<th>Enrolled On</th>
					  	</tr>
				  		${enrolleeRows}
				  	<table>

				`
				enrolleeContainer.innerHTML = enrolleeTable

			})
		}
	}
})


