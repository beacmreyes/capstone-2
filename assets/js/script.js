let navItem = document.querySelector("#navSession");
let navProfile = document.querySelector("#navProfile")
// localStorage = an object used to store information in our clients/ devices
let userToken = localStorage.getItem("token");


if (!userToken) {
    navItem.innerHTML = 
        `
            <li class="nav-item">
                <a href="./login.html" class="nav-link">
                    Login
                </a>            
            </li>

        `
    if (navProfile!=null){
        navProfile.innerHTML=""
    }
    let register=
        `         
            <li class="nav-item">
                <a href="./register.html" class="nav-link">
                    Sign Up
                </a>            
            </li>
        `
    navItem.insertAdjacentHTML('afterend', register)
} else {

	navItem.innerHTML = 
        `
            <li class="nav-item">
                <a href="./logout.html" class="nav-link">
                    Logout
                </a>            
            </li>
        `
}