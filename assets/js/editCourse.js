let params = new URLSearchParams(window.location.search)



let courseId = params.get('courseId')
console.log(params.has('courseId'))
console.log(courseId)
let token = localStorage.getItem("token");



let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDescription")
let coursePrice = document.querySelector("#coursePrice")
let editCourse = document.querySelector('#editCourse')




fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data =>{


	courseName.value=data.name
	coursePrice.value=data.price
	courseDescription.value=data.description

	editCourse.addEventListener("submit",(e) =>{
		e.preventDefault();
		if(courseName.value==data.name && coursePrice.value==data.price && courseDescription.value==data.description ){
			alert('Nothing has changed');
			window.location.replace("courses.html")
		}else{

		fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId:data._id,
				name:courseName.value,
				description: courseDescription.value,
				price: coursePrice.value	
			})
		})
		.then(res => res.json())
		.then(data =>{
			if(data==true){
				alert('Update Successful');
				window.location.replace("courses.html");
			}else{
				alert('Something went wrong');
			}

		})


		}
	})




		
})




