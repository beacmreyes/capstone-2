let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) =>{

	e.preventDefault();

	let email = document.querySelector("#userEmail").value;
	let password = document.querySelector("#password").value;
	console.log(email);
	console.log(password);

	if (email == "" || password == ""){
		alert("Please input your email and/or password")
	} else {

		fetch('https://stark-hamlet-12332.herokuapp.com/api/users/login',{
				method: 'POST',
				headers:{
					'Content-Type': 'application/json' 
				},
				body: JSON.stringify({
					email:email,
					password:password
				})
		})
		.then(res =>res.json())
		.then(data =>{

			if(data.accessToken){
				//store the token in local storage
				localStorage.setItem('token', data.accessToken);
				// localStorage{
				// 	token: data.accessToken
				// }
				fetch("https://stark-hamlet-12332.herokuapp.com/api/users/details", {
					headers:{
						'Authorization': `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(data =>{
					console.log(data)

					// store the user details in the local storage for future use
					localStorage.setItem('id',data._id);
					localStorage.setItem('isAdmin', data.isAdmin);

					//redirect the user to our courses page
					window.location.replace("courses.html")



				})

			} else {
				alert("Something went wrong")
			}
		})
	}
})