console.log(window.location.search);

// instantiate a URLSearchParams object so we can execute methods to access parameters
let params = new URLSearchParams(window.location.search)
let token = localStorage.getItem("token");

let courseId = params.get('courseId');

let deleteCourse = document.querySelector("#deleteCourse")

fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses/${courseId}`)
.then(res =>res.json())
.then(data =>{

	fetch(`https://stark-hamlet-12332.herokuapp.com/api/courses/${courseId}`,{
		method:'DELETE',
		headers:{
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data =>{
		if (data==true){
			window.location.replace("courses.html")
		}else{
			alert('Something went wrong');
			window.location.replace("courses.html")
		}
	})
	
})




